(function ($) {
	
	// Terms & Conditions
	$('body').append('<div id="popup" style="display: none;"><div id="terms-conds" style="background: white; width: 90%; height: 400px; overflow: auto; margin: auto; padding: 40px;"></div></div>');
	$('#terms-conds').load('term-conditions #main');

	var termAndCond = $('#footer-inner').find('.menu').children().first().children();

	termAndCond.click(function(e){
		e.preventDefault();
		$('#popup').bPopup();
	});
	
})(jQuery);