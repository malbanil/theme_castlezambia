(function($){
  /*
  *
  * Forms Validations
  *
  */

  var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;

  // Validate phone number function.
  function validatePhonenumber(inputtxt) {
    var phoneno = /^(?=.*[0-9])[- +()0-9]+$/;
    if (phoneno.test(inputtxt)) {
      return true;
    } else {
      return false;
    }
  }

  // Validate date function.
  function isDate(txtDate) {
    var currVal = txtDate;
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
    return false;

    //Checks for dd/mm/yyyy format.
    dtMonth = dtArray[3];
    dtDay= dtArray[1];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
    return false;
    else if (dtDay < 1 || dtDay> 31)
    return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
    return false;
    else if (dtMonth == 2) {
      var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
      if (dtDay> 29 || (dtDay ==29 && !isleap))
      return false;
    }
    return true;
  }

  // ERROR?
  function checkError(error, message) {
    if (error == true) {
      $('.error-message').html('<ul>' + message + '</ul>');
      return false;
    }
    else{
      return true;
    }
  }

  /*
  * WELCOME BACK
  */

  $('#webform-client-form-151 #edit-submitted-new-1444138658688').keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
              (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
              (e.keyCode >= 35 && e.keyCode <= 40)) {
          return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });

  jQuery('#webform-client-form-151').submit(function (event) {

    var error = false;
    var message = '';

    //validate_fields();
    //NOMBRE
    if ($('#edit-submitted-new-1444138438904').val().trim() == '') {
      message += '<li class="validate_fist_name">Name is required.</li>';
      error = true;
    }
    //TELEFONO
    if ($('#edit-submitted-new-1444138658688').val().trim() == '') {
      message += '<li class="validate_telefono">Cell Number is required.</li>';
      error = true;
    } else if (!validatePhonenumber($('#edit-submitted-new-1444138658688').val().trim())) {
      message += '<li class="validate_telefono">Cell Number format is not valid.</li>';
      error = true;
    }
    //CORREO
    if (!regex.test($('#edit-submitted-new-1444138755945').val().trim()) && $('#edit-submitted-new-1444138755945').val().trim() !== '') {
      message += '<li class="validate_correo_no valido">Email Address format is not valid.</li>';
      error = true;
    }
    //LOCATION
    if ($('#edit-submitted-new-1444163383260 option:selected').val() == 'one') {
      message += '<li class="validate_location">Location is required.</li>';
      error = true;
    }
    //DATE OF BIRTH
    if ($('#edit-submitted-new-1444138980451').val().trim() == '') {
      message += '<li class="validate_fecha_no_valida">Date of Birth is required.</li>';
      error = true;
    } else if (!isDate($('#edit-submitted-new-1444138980451').val().trim())) {
      message += '<li class="validate_fecha_no_valida">Date of Birth format is not valid.</li>';
      error = true;
    }
    // Terms and conditions
    if (!$('#edit-submitted-new-1444139112260-new-1444139118794-2').is(':checked')) {
      message += '<li class="validate_fecha_no_valida">You must accept the terms and conditions.</li>';
      error = true;
    }

    if(checkError(error, message)){
      return true;
    }
    else{
      event.preventDefault();
      return false;
    }
  });

  /*
  * APPLY FOR CITIZEN
  */

  $('#webform-client-form-161 #edit-submitted-new-1444140048014').keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
              (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
              (e.keyCode >= 35 && e.keyCode <= 40)) {
          return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });

  jQuery('#webform-client-form-161').submit(function (event) {

    var error = false;
    var message = '';

    //validate_fields();
    //NOMBRE
    if ($('#edit-submitted-new-1444139824994').val().trim() == '') {
      message += '<li class="validate_fist_name">Name is required.</li>';
      error = true;
    }
    //APELLIDO
    if ($('#edit-submitted-new-1444140032327').val().trim() == '') {
      message += '<li class="validate_surname">Surname is required.</li>';
      error = true;
    }
    //TELEFONO
    if ($('#edit-submitted-new-1444140048014').val().trim() == '') {
      message += '<li class="validate_telefono">Cell Number is required.</li>';
      error = true;
    } else if (!validatePhonenumber($('#edit-submitted-new-1444140048014').val().trim())) {
      message += '<li class="validate_telefono">Cell Number format is not valid.</li>';
      error = true;
    }
    //CORREO
    if ($('#edit-submitted-new-1444140049598').val().trim() == '') {
      message += '<li class="validate_correo">Email address is required.</li>';
      error = true;
    }
    else if (!regex.test($('#edit-submitted-new-1444140049598').val().trim())) {
      message += '<li class="validate_correo_no valido">Email Address format is not valid.</li>';
      error = true;
    }
    //LOCATION
    if ($('#edit-submitted-new-1444140336948 option:selected').val() == 'one') {
      message += '<li class="validate_location">Location is required.</li>';
      error = true;
    }
    //GENDER
    if ($('#edit-submitted-new-1444140445315 option:selected').val() == 'one') {
      message += '<li class="validate-gender">Gender is required.</li>';
      error = true;
    }
    //DATE OF BIRTH
    if ($('#edit-submitted-new-1444140089214').val().trim() == '') {
      message += '<li class="validate_fecha_no_valida">Date of Birth is required.</li>';
      error = true;
    } else if (!isDate($('#edit-submitted-new-1444140089214').val().trim())) {
      message += '<li class="validate_fecha_no_valida">Date of Birth format is not valid.</li>';
      error = true;
    }
    // Terms and conditions
    if (!$('#edit-submitted-new-1444140738627-2').is(':checked')) {
      message += '<li class="validate_fecha_no_valida">You must accept the terms and conditions.</li>';
      error = true;
    }

    if(checkError(error, message)){
      return true;
    }
    else{
      event.preventDefault();
      return false;
    }
  });

  /*
  * QUESTIONNAIRE
  */

  $('#webform-client-form-171 #edit-submitted-new-1444143256567 input').click(function(){
      if ($('#edit-submitted-new-1444143256567').find('input:checked').length >= 3) {
        $('#edit-submitted-new-1444143256567 input').each(function(){
          if($(this).is('input:checked') == false){
            $(this).parent().find('label').addClass('disable');
            $(this).attr('disabled','disabled');
          }
        });
      }
      else {
        $('#edit-submitted-new-1444143256567 input').each(function(){
          $(this).parent().find('label').removeClass('disable');
          $(this).removeAttr('disabled');
        });
      }
  });

  $('#webform-client-form-171').submit(function(event){
    var message = '';
    var error = false;
    if($('#edit-submitted-new-1444143256567').find('input:checked').length == 0){
      message += '<li class="validate_option">You must select a option.</li>';
      error = true;
    }
    if(checkError(error, message)){
      return true;
    }
    else{
      event.preventDefault();
      return false;
    }
  });

})(jQuery);
