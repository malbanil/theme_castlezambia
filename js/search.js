(function ($) {
	var widthScreen = $(window).width();
	var searchForm = $('#block-search-form');

	$(document).ready(function() {
		if(widthScreen >= 992){
			searchForm.appendTo($('#navigation #block-system-main-menu').find('.menu-dropdown-js-enabled')).wrap('<li></li>');
			searchForm.parent().addClass('search-desktop');
		}
		else {
			searchForm.prependTo($('#navigation'));
		}
	});


	var mobile = true;
	if(widthScreen >= 992){
		mobile = false;
	}



	$(window).resize(function(){
		widthScreen = $(window).width();

		if(widthScreen >= 992){
			if (mobile){
				searchForm.appendTo($('#navigation #block-system-main-menu').find('.menu-dropdown-js-enabled')).wrap('<li></li>');
				searchForm.parent().addClass('search-desktop');
				mobile = false;
			}
		}
		else {
			if (!mobile){
				searchForm.prependTo($('#navigation'));
				$('#navigation #block-system-main-menu').find('.search-desktop').remove();
				mobile = true;
			}
		}
	});
})(jQuery);
