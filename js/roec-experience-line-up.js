/*
 * roec-experience-line-up.js
 *
 */
(function($){

	$(window).load(function(){
    if($('body').hasClass('page-experience-line-up')){
			
			// Scroll personalizado del bloque .scroll
      $(".scroll").customScrollbar({updateOnWindowResize:true});

			// Grilla de imagenes
			$('.block-right img').each(function(indice, elemento){
				var imagenSrc = $(elemento).attr('src');
				$(elemento).parent().css({
					'background-image': 'url("'+imagenSrc+'")',
					'background-size': 'cover',
					'background-position': 'center center'
				});
			});

		}
  });
})(jQuery);
