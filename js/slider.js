(function ($) {
	// Ice Core About Slide
	var widthScreen = $(window).width();
	var	slideContainer = $('.view-ice-core-about').find('.item-list');

	slideContainer.clone().insertAfter(slideContainer);
	slideContainer.first().addClass('desktop');
	slideContainer.first().next().addClass('mobile');
	$('.view-ice-core-about').find('ul').addClass('slides');

	sliderLandscape();
	sliderPortrait();

	$(window).load(function() {
		if(widthScreen >= 992){
			$('.item-list.mobile').hide();
		}
		else {
			$('.item-list.desktop').hide();
		}
	});
	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('.item-list.mobile').hide();
			$('.item-list.desktop').show();

		}
		else {
			$('.item-list.desktop').hide();
			$('.item-list.mobile').show();

		}
	});
	function sliderLandscape() {
		$('.item-list.desktop').flexslider({
			slideshow: false,
			animation: "slide",
			direction: "vertical",
			directionNav: false
		});
	}
	function sliderPortrait() {
		$('.item-list.mobile').flexslider({
			slideshow: false,
			animation: "slide",
			direction: "horizontal",
			controlNav: false
		});
	}


	// Función para el scroll
    var numberUp = 0;
	var numberDown = 0;
	function clickScroll(){
		clearTimeout($.data(this, 'timer'));
	    $.data(this, 'timer', setTimeout(function() {
	      element = $('.desktop .flex-control-nav li a.flex-active').parent().next();
	      $(element).find('a').trigger('click');
	    }, 250));
	}

	var wheelDesk = function(event){
		//$('.view-ice-core-about .desktop').mouseover(function(){
			if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
		      numberUp = numberUp + 1;
		      if (numberUp >= 5){
		        clearTimeout($.data(this, 'timer'));
		        $.data(this, 'timer', setTimeout(function() {
		          element = $('.desktop .flex-control-nav li a.flex-active').parent().prev();
		          $(element).find('a').trigger('click');
		          numberUp = 0;
		        }, 250));
		      }
		    }
		    else {
		      numberDown = numberDown + 1;
		      if (numberDown >= 5){
		        clearTimeout($.data(this, 'timer'));
		        $.data(this, 'timer', setTimeout(function() {
		          element = $('.desktop .flex-control-nav li a.flex-active').parent().next();
		          $(element).find('a').trigger('click');
		          numberDown = 0;
		        }, 250));
		      }
		    }
		    if ($('.desktop .flex-control-nav li a').length){
		      return false;
		    }
		//});
	}

	var widthScreen = $(window).width();
	$(document).ready(function() {
		$('.view-ice-core-about .desktop .ice-core-about-container').bind('mousewheel DOMMouseScroll', wheelDesk);
		if(widthScreen <= 992){
			$('.view-ice-core-about .desktop .ice-core-about-container').unbind('mousewheel DOMMouseScroll');
		}
		$('.item-list.desktop .scroll').click(function(){
			clickScroll();
		})
	});
	$(window).resize(function(){
		widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('.view-ice-core-about .desktop .ice-core-about-container').bind('mousewheel DOMMouseScroll', wheelDesk);
		}
		else {
			$('.view-ice-core-about .desktop .ice-core-about-container').unbind('mousewheel DOMMouseScroll');
		}
	});
})(jQuery);
