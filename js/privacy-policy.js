(function($){
	/*
	 *
	 * Privacy Policy link
	 *
	 */
	$( document ).ready(function() {
		var pdf = $('#privacy-policy-ref').data('file');
		var xternal = $('#privacy-policy-ref').data('external');
		var tipoLink = $('#privacy-policy-ref').data('tipo');
		var privacyMenuLink = $('.menu-link-2546').children();
		privacyMenuLink.attr('target', '_blank');
		
		if(tipoLink == 'External link') {
			privacyMenuLink.attr('href', xternal);
		}
		if(tipoLink == 'File') {
			privacyMenuLink.attr('href', pdf);
		}
	});
})(jQuery);