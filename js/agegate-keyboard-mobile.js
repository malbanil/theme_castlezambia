(function($){
	if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
        $('#age_checker_day').replaceWith($('#age_checker_day').clone().attr('type', 'tel'));
        $('#age_checker_month').replaceWith($('#age_checker_month').clone().attr('type', 'tel'));
        $('#age_checker_year').replaceWith($('#age_checker_year').clone().attr('type', 'tel'));
    }
})(jQuery);