(function($){
	function icresize(){
		if($('body').hasClass('page-ice-core')){
			var h = $('.view-display-id-page_ice_core .view-content').height()+150
			$("#content").height(h)
		}
		if($('body').hasClass('page-ice-core-buy-online')){
			var h = $('.view-display-id-page_ice_core_buy_online .view-content').height()+50
			$("#content").height(h)
		}
	}
	$(window).load(function() {
		icresize();
	});
	// Ice Core
	var bkg_landscape_ice_core = $('.landing-page-container').data('bkg-land');
	var bkg_portrait_ice_core = $('.landing-page-container').data('bkg-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-ice-core #page #content').css('background-image','url("'+bkg_landscape_ice_core+'")');
	}
	else{
		$('body.page-ice-core #page #content').css('background-image','url("'+bkg_portrait_ice_core+'")');
	}
	$(window).resize(function(){
		icresize();
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-ice-core #page #content').css('background-image','url("'+bkg_landscape_ice_core+'")');
		}
		else{
			$('body.page-ice-core #page #content').css('background-image','url("'+bkg_portrait_ice_core+'")');
		}
	});


	//Ice Core Buy Online
	var bkg_landscape_ice_core_buy = $('.buy-online-container').data('bkg-land');
	var bkg_portrait_ice_core_buy = $('.buy-online-container').data('bkg-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-ice-core-buy-online #page #content').css('background-image','url("'+bkg_landscape_ice_core_buy+'")');
	}
	else{
		$('body.page-ice-core-buy-online #page #content').css('background-image','url("'+bkg_portrait_ice_core_buy+'")');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-ice-core-buy-online #page #content').css('background-image','url("'+bkg_landscape_ice_core_buy+'")');
		}
		else{
			$('body.page-ice-core-buy-online #page #content').css('background-image','url("'+bkg_portrait_ice_core_buy+'")');
		}
	});

    //Ice Core About
    $('body.page-ice-core-about .desktop').css('background-image','url("/sites/g/files/ogq1826/themes/site/images/fondo_ice_core.jpg")');
    $('body.page-ice-core-about .mobile').css('background-image','url("/sites/g/files/ogq1826/themes/site/images/ice-core-mob2.jpg")');


    //ROEC Home
	var bkg_landscape_roec_home = $('.container-roec-home').data('back-land');
	var bkg_portrait_roec_home = $('.container-roec-home').data('back-port');

	var widthScreen = $(window).width();
	if(!$('body').hasClass('page-roec-terms-conditions')) {
		if(widthScreen >= 992){
			$('body.page-roec #page #content').css('background-image','url("'+bkg_landscape_roec_home+'")');
			$('body.page-roec #page #content .back-port').css('background-image','none');
		}else{
			$('body.page-roec #page #content .back-port').css('background-image','url("'+bkg_portrait_roec_home+'")');
			$('body.page-roec #page #content').css('background-image','none');
		}
	}else{
		$('body').removeClass('page-roec');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(!$('body').hasClass('page-roec-terms-conditions')) {
			if(widthScreen >= 992){
				$('body.page-roec #page #content').css('background-image','url("'+bkg_landscape_roec_home+'")');
				$('body.page-roec #page #content .back-port').css('background-image','none');
			}
			else{
				$('body.page-roec #page #content .back-port').css('background-image','url("'+bkg_portrait_roec_home+'")');
				$('body.page-roec #page #content').css('background-image','none');
			}
		}
	});


	//Welcome Back
	var bkg_landscape_welcome_back = $('.container-welcome-back').data('back-land');
	var bkg_portrait_welcome_back = $('.container-welcome-back').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-welcome-back #page #content').css('background-image','url("'+bkg_landscape_welcome_back+'")');
		$('body.page-welcome-back #page #content .back-port').css('background-image','none');
	}
	else{
		$('body.page-welcome-back #page #content .back-port').css('background-image','url("'+bkg_portrait_welcome_back+'")');
		$('body.page-welcome-back #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-welcome-back #page #content').css('background-image','url("'+bkg_landscape_welcome_back+'")');
			$('body.page-welcome-back #page #content .back-port').css('background-image','none');
		}
		else{
			$('body.page-welcome-back #page #content .back-port').css('background-image','url("'+bkg_portrait_welcome_back+'")');
			$('body.page-welcome-back #page #content').css('background-image','none');
		}
	});


	//Apply for Citizen
	var bkg_landscape_apply_citizen = $('.container-apply-citizen').data('back-land');
	var bkg_portrait_apply_citizen = $('.container-apply-citizen').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-apply-for-citizen #page #content').css('background-image','url("'+bkg_landscape_apply_citizen+'")');
		$('body.page-apply-for-citizen #page #content .back-port').css('background-image','none');
	}
	else{
		$('body.page-apply-for-citizen #page #content .back-port').css('background-image','url("'+bkg_portrait_apply_citizen+'")');
		$('body.page-apply-for-citizen #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-apply-for-citizen #page #content').css('background-image','url("'+bkg_landscape_apply_citizen+'")');
			$('body.page-apply-for-citizen #page #content .back-port').css('background-image','none');
		}
		else{
			$('body.page-apply-for-citizen #page #content .back-port').css('background-image','url("'+bkg_portrait_apply_citizen+'")');
			$('body.page-apply-for-citizen #page #content').css('background-image','none');
		}
	});


	//Questionnaire
	var bkg_landscape_questionnaire = $('.container-questionnaire').data('back-land');
	var bkg_portrait_questionnaire = $('.container-questionnaire').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-questionnaire #page #content').css('background-image','url("'+bkg_landscape_questionnaire+'")');
		$('body.page-questionnaire #page #content .back-port').css('background-image','none');
	}
	else{
		$('body.page-questionnaire #page #content .back-port').css('background-image','url("'+bkg_portrait_questionnaire+'")');
		$('body.page-questionnaire #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-questionnaire #page #content').css('background-image','url("'+bkg_landscape_questionnaire+'")');
			$('body.page-questionnaire #page #content .back-port').css('background-image','none');
		}
		else{
			$('body.page-questionnaire #page #content .back-port').css('background-image','url("'+bkg_portrait_questionnaire+'")');
			$('body.page-questionnaire #page #content').css('background-image','none');
		}
	});


	//Buy Tickets
	var bkg_landscape_buy_tickets = $('.container-buy-tickets').data('back-land');
	var bkg_portrait_buy_tickets = $('.container-buy-tickets').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-buy-tickets #page #content').css('background-image','url("'+bkg_landscape_buy_tickets+'")');
		$('body.page-buy-tickets #page #content .back-port').css('background-image','none');
	}
	else{
		$('body.page-buy-tickets #page #content .back-port').css('background-image','url("'+bkg_portrait_buy_tickets+'")');
		$('body.page-buy-tickets #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-buy-tickets #page #content').css('background-image','url("'+bkg_landscape_buy_tickets+'")');
			$('body.page-buy-tickets #page #content .back-port').css('background-image','none');
		}
		else{
			$('body.page-buy-tickets #page #content .back-port').css('background-image','url("'+bkg_portrait_buy_tickets+'")');
			$('body.page-buy-tickets #page #content').css('background-image','none');
		}
	});



	//Thanks Apply for Citizen
	var bkg_landscape_thank_apply_citizen = $('.container-thank-apply_citizen').data('back-land');
	var bkg_portrait_thank_apply_citizen = $('.container-thank-apply_citizen').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-thank-you-apply-for-citizen #page #content').css('background-image','url("'+bkg_landscape_thank_apply_citizen+'")');
		$('body.page-thank-you-apply-for-citizen #page #content .back-port').css('background-image','none');
	}
	else{
		$('body.page-thank-you-apply-for-citizen #page #content .back-port').css('background-image','url("'+bkg_portrait_thank_apply_citizen+'")');
		$('body.page-thank-you-apply-for-citizen #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-thank-you-apply-for-citizen #page #content').css('background-image','url("'+bkg_landscape_thank_apply_citizen+'")');
			$('body.page-thank-you-apply-for-citizen #page #content .back-port').css('background-image','none');
		}
		else{
			$('body.page-thank-you-apply-for-citizen #page #content .back-port').css('background-image','url("'+bkg_portrait_thank_apply_citizen+'")');
			$('body.page-thank-you-apply-for-citizen #page #content').css('background-image','none');
		}
	});

	//Thank you for sign in
	var bkg_landscape_thank_signin = $('.container-thank-apply_citizen').data('back-land');
	var bkg_portrait_thank_signin = $('.container-thank-apply_citizen').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-thank-you-for-signing-in #page #content').css('background-image','url("'+bkg_landscape_thank_signin+'")');
		$('body.page-thank-you-for-signing-in #page #content .back-port').css('background-image','none');
	}
	else{
		$('body.page-thank-you-for-signing-in #page #content .back-port').css('background-image','url("'+bkg_portrait_thank_signin+'")');
		$('body.page-thank-you-for-signing-in #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-thank-you-for-signing-in #page #content').css('background-image','url("'+bkg_landscape_thank_signin+'")');
			$('body.page-thank-you-for-signing-in #page #content .back-port').css('background-image','none');
		}
		else{
			$('body.page-thank-you-for-signing-in #page #content .back-port').css('background-image','url("'+bkg_portrait_thank_signin+'")');
			$('body.page-thank-you-for-signing-in #page #content').css('background-image','none');
		}
	});

	//Thank for entering
	var bkg_landscape_thanks_entering = $('.container-thank-apply_citizen').data('back-land');
	var bkg_portrait_thanks_entering = $('.container-thank-apply_citizen').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-thanks-for-entering #page #content').css('background-image','url("'+bkg_landscape_thanks_entering+'")');
		$('body.page-thanks-for-entering #page #content .back-port').css('background-image','none');
	}
	else{
		$('body.page-thanks-for-entering #page #content .back-port').css('background-image','url("'+bkg_portrait_thanks_entering+'")');
		$('body.page-thanks-for-entering #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-thanks-for-entering #page #content').css('background-image','url("'+bkg_landscape_thanks_entering+'")');
			$('body.page-thanks-for-entering #page #content .back-port').css('background-image','none');
		}
		else{
			$('body.page-thanks-for-entering #page #content .back-port').css('background-image','url("'+bkg_portrait_thanks_entering+'")');
			$('body.page-thanks-for-entering #page #content').css('background-image','none');
		}
	});

	// Republic contact
	var bkg_landscape_republic_contact = $('.contact-container').data('back-land');
	var bkg_portrait_republic_contact = $('.contact-container').data('back-port');

	var widthScreen = $(window).width();
	if(widthScreen >= 992){
		$('body.page-republic-contact #page #content').css('background-image','url("'+bkg_landscape_republic_contact+'")');
	}
	else{
		$('body.page-republic-contact #page #content').css('background-image','none');
	}

	$(window).resize(function(){
		var widthScreen = $(window).width();
		if(widthScreen >= 992){
			$('body.page-republic-contact #page #content').css('background-image','url("'+bkg_landscape_republic_contact+'")');
		}
		else{
			$('body.page-republic-contact #page #content').css('background-image','none');
		}
	});

})(jQuery);
