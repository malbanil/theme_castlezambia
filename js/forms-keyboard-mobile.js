(function($){
  /*
  *
  * Forms Keyboard Mobile
  *
  */
  if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
    $('#edit-submitted-new-1444138980451').replaceWith($('#edit-submitted-new-1444138980451').clone().attr('type', 'tel'));
    $('#edit-submitted-new-1444140089214').replaceWith($('#edit-submitted-new-1444140089214').clone().attr('type', 'tel'));

  	$('#edit-submitted-new-1444138980451, #edit-submitted-new-1444140089214').keyup(function(e){
    	if($.inArray(e.keyCode, [46, 8, 9, 27, 13]) === -1){
    		var cant = $(this).val().length;
			var value = $(this).val();
			if(cant == 2 || cant == 5){
				$(this).val(value + '/');
			}
			if(cant == 3 && $(this).val().charAt(2) != '/'){
				var lastChar = value.substr(value.length - 1);
				$(this).val(value.replaceAt(2, '/') + lastChar);
			}
			if(cant == 6 && $(this).val().charAt(5) != '/'){
				var lastChar = value.substr(value.length - 1);
				$(this).val(value.replaceAt(5, '/') + lastChar);
			}
    	}
	});
  
  }


})(jQuery);

String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
}
