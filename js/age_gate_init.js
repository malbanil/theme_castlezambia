(function($) {
      // Extendiendo la funcionalidad de la inicializacion del age gate
      areThereAgeGate = false;
      age_checker.init = (function(old_init) {
          function extendsInit(context, settings) {

              var old_init_response = old_init(context, settings);
              // Ocultando el contenido del sitio si se muestra el age gate
              if (jQuery.cookie("age_checker") == null) {
                areThereAgeGate = true;
                $(window).load(function(){
                  if(areThereAgeGate) {
                    jQuery('#page').addClass('hidden');
                  }
                });
                //window.setTimeout(function(){}, 5000);
              }
              return old_init_response;
          }
          return extendsInit;
      })(age_checker.init);
      // Extendiendo la funcionalidad de la verificacion del age gate
      age_checker.verify = (function(old_verify) {
          function extendsVerify() {
            var old_verify_response = old_verify();
            // Mostrando el contenido del sitio si la edad es valida
            if(old_verify_response){
              areThereAgeGate = false;
              jQuery('#page').removeClass('hidden');
            }
            return old_verify_response;
          }
          return extendsVerify;
      })(age_checker.verify);
      // Agregando nuevamente la funcion al bloque de Drupal
      Drupal.behaviors.age_checker = {
          attach: age_checker.init
      }
})(jQuery);
