/*Menu responsive*/
(function ($) {
	$(document).ready(function(){

		// zindex del submenu
		submenuZindex = 1;

		// Esconder submenus correctamente
		$('.menu .menu').css('visibility','initial');

		// Menues (elementos li del menu)
		menues = $('#block-system-main-menu li')

		//Remover eventos previamente asignados al menu
		menues.each(function(){
			$(this).unbind();
		});

		/*
		 *
		 * Menu Responsive
		 *
		 */

		var botonMenuResponsive = $('#navigation .stack-width.inset-1.inset.tb-terminal');

		// Mostrar y esconder menu responsive
		botonMenuResponsive.bind( "click", function() {
		  botonMenuResponsive.toggleClass('desplegado');
			$('#content').toggleClass('hidden');
			$('#footer').toggleClass('hidden');
		});

		/*
		 *
		 * Social Responsive
		 *
		 */

		var socialMedia = $('.view-social-media');

		// Mover Social menu al cargar en mobile
		var widthScreen = $(window).width();
		$(document).ready(function() {
			if(widthScreen <= 992){
				socialMedia.prependTo($('#navigation'));
				socialMedia.addClass('social-media-mobile');
			}
		});

		// Mover Social menu al cambiar tamano de pantalla
		var mobile = true;
		if(widthScreen >= 992){
			mobile = false;
		}

		$(window).resize(function(){
			widthScreen = $(window).width();

			if(widthScreen >= 992){
				if (mobile){
					socialMedia.appendTo($('#block-views-social-media-block-social-media').find('.content'));
					mobile = false;
				}
			}
			else {
				if (!mobile){
					socialMedia.prependTo($('#navigation'));
					socialMedia.addClass('social-media-mobile');
					mobile = true;
				}
			}
		});

		var botonSocialResponsive = $('.social-media-mobile');

		// Mostrar y esconder menu responsive
		botonSocialResponsive.bind( "click", function() {
		  botonSocialResponsive.toggleClass('desplegado');
		});

		// Mostrar y esconder submenu responsive
		botonMenuResponsive.find('.more-indicator').each(function(){
			$(this).click(function(event){
				event.preventDefault();
				event.stopPropagation();
				$(this).parent().parent().toggleClass('desplegado');
			});
		});

		/*
		 *
		 * Menu desktop
		 *
		 */

		 //comportamieto mouse enter / leave
		 menues.each(function(){
			 var elementoActual = $(this);
			 elementoActual.bind({
				 mouseenter : function(){
					window.clearTimeout(this.objetoTimeout);
					if ( elementoActual.hasClass('level-1') ){
						$(window.ultimoDesplegado).children('ul.menu').removeClass('desplegado');
					}
					if ( elementoActual.hasClass('expanded') ){
						window.ultimoDesplegado = this;
					}
					elementoActual.children('ul.menu').addClass('desplegado');
					elementoActual.children('ul.menu').css('z-index', submenuZindex);
					submenuZindex++;
					},

					mouseleave : function(){
						this.objetoTimeout = window.setTimeout(function(elem){
							elem.children('ul.menu').removeClass('desplegado');
						}, 1000, elementoActual);
				}
			});

			// Quitar decoracion del enlace siguiente al activo
			if(elementoActual.hasClass('active-trail')){
				elementoActual.next().addClass('active-trail-next');
			}

		});

	});

})(jQuery);
