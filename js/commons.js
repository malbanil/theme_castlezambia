(function($){
	$(document).ready(function(){
		/** Age checkerLimpiar estilos*/
		$('#age_checker_verification_popup').removeAttr( 'style' );

		//add viewport
		//$('head').append('<meta name="viewport" content="width=device-width, initial-scale=1.0">')

		//logo
		$('#header-inner .tb-header-inner-2').click(function(){
			pathArray = location.href.split( '/' );
			protocol = pathArray[0];
			host = pathArray[2];
			url = protocol + '//' + host;
			window.location.href = url;
		});

		//add class boostrap
		if($('body').hasClass('page-ice-core')){
			$('.second-title').html('THE NEXT GENERATION IN <br>EXTRA COLD')
			$("#content").addClass("container-fluid");
			$('.view-display-id-page_ice_core').addClass('container');
			$('.view-display-id-page_ice_core .view-content').addClass('col-md-6 col-md-offset-6 col-sm-12');
			$(".latas").append("<img src='/sites/g/files/ogq1826/f/201509/latas.png' class='img-responsive'>");
		}
		if($('body').hasClass('page-ice-core-buy-online')){
			$("#content").addClass("container-fluid");
			$('.view-display-id-page_ice_core_buy_online').addClass('container');
			$('.view-display-id-page_ice_core_buy_online .view-content').addClass('col-md-6 col-sm-12');
			$(".latas").append("<img src='/sites/g/files/ogq1826/f/201509/latas.png' class='img-responsive'>");
		}
		if($('body').hasClass('page-contact')){
			$("#content").addClass("container-fluid");
			$('.view-id-contact').addClass('container');
			$('.view-id-contact .view-content').addClass('col-sm-12');
		}
		if($('body').hasClass('page-ice-core-about')){
			$(".view-ice-core-about .slides li").addClass("container-fluid");
			$(".latas").append("<img src='/sites/g/files/ogq1826/f/201509/latas.png' class='img-responsive'>");
		}
		if($('body').hasClass('page-contact')){
			$("#content").addClass("container-fluid");
			$('.view-id-contact').addClass('container');
			$('.view-id-contact .view-content').addClass('col-sm-12');
		}

		if($('body').hasClass('page-experience-line-up')){
			$("#content").addClass("container-fluid");
			$('.view-id-experience_line_up').addClass('container-fluid');
			$('.view-id-experience_line_up .view-content').addClass('col-sm-12');
			//hexpe();
		}
		if($('body').hasClass('page-roec')){
			$("#content").addClass("container-fluid");
			// $('.view-id-roec_home.view-display-id-page').parent().addClass('container');
			$('.view-id-roec_home.view-display-id-page').addClass('col-md-6 col-md-offset-5 col-sm-12 text-center');
		}

		// Add classes to footer main menu
		$('#prefooter').find('.box').children().removeClass();
		//$('#prefooter').find('.box').children().addClass('col-md-12');
		$('#prefooter').find('#prefooter-first-region').parent().addClass('linksFooter');
		$('#prefooter').find('#prefooter-first-region').addClass('container');
		$('#prefooter').find('#prefooter-third-region').parent().addClass('socialMediaFooter');
		$('.footer-main-menu').find('.content').children().children().addClass('menu-title');
		$('.menu-title').find('li').addClass('menu-links');
		$('#footer-inner').find('.inset-4').addClass('container');

		var widthScreen = $(window).width();
		var iphonem = ( /iPhone/i.test(navigator.userAgent))
		if((widthScreen == 375) && (iphonem)){
			setTimeout(function(){
				$('.item-list.mobile ul.slides').css('-webkit-transform','translate3d(-375px, 0px, 0px)');
			}, 1000);
		}


		// Images people in forms
		var image_mobile_1 = $('.image01').html();
		var image_mobile_2 = $('.image02').html();
		if(image_mobile_1 != null && image_mobile_2 != null){
			var content_desk = '<div class="images-desk">' + image_mobile_1 + image_mobile_2 + '</div>';
			$('.block-blue').append(content_desk);
		}

	});

	/*
	setTimeout(function(){
		hexpe();
	}, 1000);

	$(window).resize(function(){
		hexpe();
	});

	function hexpe(){
		if (!isExperienceLineUp){
			widthScreen = $(window).width();
			if(widthScreen >= 1200){
				var hw = $('.block-right').height();
				$('.block-left').css('height', hw);
			}
		}
	}
	*/
})(jQuery);
