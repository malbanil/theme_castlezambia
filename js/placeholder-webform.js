(function($){
	/*
	 *
	 * Placeholder webform
	 *
	 */
	// Read value of each input text and set it to the placeholder
	$('form.webform-client-form input').each(function(index, value){
		if($(this).is("[type=text]")){
			$(this).attr('placeholder',  $(value).val());
			$(this).removeAttr('value');
		}
	});

	// On page refresh read the placeholder and set it to the value
	$( window ).unload(function() {
		$('form.webform-client-form input').each(function(index, value){
			if($(this).is("[type=text]")){
				$(this).val($(this).attr('placeholder'));
			}
		});
	});

	$('form.webform-client-form select option:first-child').each(function(index, value){
		$(this).attr('disabled', 'disabled');
		$(this).attr('selected', 'selected');
	});

	$('form.webform-client-form input').placeholder({ customClass: 'my-placeholder' });
})(jQuery);
