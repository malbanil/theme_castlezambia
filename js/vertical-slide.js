(function ($) {
    // Ice Core About Slide
    var slideContainer = $('.view-ice-core-about').find('.item-list');
    slideContainer.children().addClass('slides');
    $(window).load(function() {
         slideContainer.flexslider({
            slideshow: false,
            animation: "slide",
            direction: "vertical",
            directionNav: false
         });
    });
})(jQuery);