(function($){
	/*
	 * Poner a los slides el height 100%
	 */
	// Calcular alto de pantalla y menu.
	var windowHeight = $(window).height();
	var menuHeight = $('#header').height();
	var firstSlide = windowHeight - menuHeight;
	
	// Aplicarlo a los slides
	var lis = $('.desktop').find('.slides').children();
	$( window ).load(function() {
		lis.not(":first").css('height', windowHeight + 'px');
		lis.first().css('height', firstSlide + 'px');
	});
	
	
	// Agregar Nav vertical
	$('.page-width.tb-scope').attr('data-ref', '0');
	$('#footer > div > div').attr('data-ref', 'bottom');
	var navLinks = '<ul class="nav-vertical">';
	navLinks += '<li style="display: none;"><a href="#" class="linkNav top" data-ref="0">-1</a></li>';
	$('.view-ice-core-about .desktop ul.slides li').each(function(index, value){
		var referencia = $(this).find('.ice-core-about-container').data('ref');
		if(index == 0){
			navLinks += '<li><a href="#" class="linkNav active" data-ref="'+referencia+'">'+index+'</a></li>';
		}
		else{
			navLinks += '<li><a href="#" class="linkNav" data-ref="'+referencia+'">'+index+'</a></li>';
		}
	});
	navLinks += '<li style="display: none;"><a href="#" class="linkNav bottom" data-ref="bottom">-1</a></li>';
	navLinks += '</ul>';
	$('.view-ice-core-about').before(navLinks);
	
	// calcular alto de Nav vertical y aplicarlo.
	var navVertical = $('.nav-vertical').height();
	console.info(navVertical);
	$('.nav-vertical').css('height', '40px');

	// Agregar click function a Nav vertical
	$('ul.nav-vertical li a').click(function (e) {
		e.preventDefault();
		var linkSelected = $(this).data('ref');
		//removeActive();
		//$(this).addClass('active');
		var optSelected = null
		$('.view-ice-core-about .desktop ul.slides li .ice-core-about-container, .page-width.tb-scope, #footer > div > div').each(function(index, value){
			if(linkSelected == $(this).data('ref')){
				optSelected = $(this).parent().parent();
			}
		});
        $('html, body').animate({
            scrollTop: optSelected.offset().top
        }, 500);
        return false;
    });
	
	$(window).scroll(function(){
    	var scrollTop = $(this).scrollTop();
    	$('.desktop .ice-core-about-container').each(function(){
    		var distanceTop = $(this).parent().parent().offset().top;
    		var distanceBottom = distanceTop + $(this).parent().parent().height();
			distanceBottom = distanceBottom - windowHeight * 0.25;
			distanceTop = distanceTop - windowHeight * 0.2;
    		if($(window).scrollTop() >= distanceTop && $(window).scrollTop() <= distanceBottom){
    			removeActive();
    			printActive($(this).data('ref'));
    		}
    	});
	});

    function removeActive(){
    	$('ul.nav-vertical li a').each(function(){
    		$(this).removeClass('active');
    	});
    }

    function printActive(ref){
    	$('ul.nav-vertical li a').each(function(){
    		if(ref == $(this).data('ref')){
    			$(this).addClass('active');
    		}
    	});
    }

	// Función para el scroll
    var numberUp = 0;
	var numberDown = 0;
	var wheelDesk = function(event){  
	    if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
	      numberUp = numberUp + 1;
	      if (numberUp >= 5){
	        clearTimeout($.data(this, 'timer'));
	        $.data(this, 'timer', setTimeout(function() {
	          element = $('a.linkNav.active').parent().prev();
	          $(element).find('a').trigger('click');
	          numberUp = 0;
	        }, 250));
	      }
	    }
	    else {
	      numberDown = numberDown + 1;
	      if (numberDown >= 5){
	        clearTimeout($.data(this, 'timer'));
	        $.data(this, 'timer', setTimeout(function() {
	          element = $('a.linkNav.active').parent().next();
	          $(element).find('a').trigger('click');
	          numberDown = 0;
	        }, 250));
	      }
	    }
	    if ($('a.linkNav').length){
	      return false;
	    }
	}
	
	var widthScreen = $(window).width();
	$(document).ready(function() {
		$(window).bind('mousewheel DOMMouseScroll', wheelDesk);
		if(widthScreen <= 992){
			$(window).unbind('mousewheel DOMMouseScroll');
		}
	});
	$(window).resize(function(){
		widthScreen = $(window).width();
		if(widthScreen >= 992){
			$(window).bind('mousewheel DOMMouseScroll', wheelDesk);

		}
		else {
			$(window).unbind('mousewheel DOMMouseScroll');
		}
	});

	$(document).keydown(function(e) {
	    switch(e.which) {
	        case 38: // up
	          element = $('a.linkNav.active').parent().prev();
	          $(element).find('a').trigger('click');
	        break;

	        case 40: // down
	          element = $('a.linkNav.active').parent().next();
	          $(element).find('a').trigger('click');
	        break;

	        default: return; // exit this handler for other keys
	    }
	    e.preventDefault(); // prevent the default action (scroll / move caret)
	});


	/*var ts;
	var numberUpMovil = 0;
	var numberDownMovil = 0;
	$('.view-ice-core-about .view-content').bind('touchstart', function (e){
	    ts = e.originalEvent.touches[0].clientY;
	});
	$('.view-ice-core-about .view-content').bind('touchmove', function (e){
	    var te = e.originalEvent.changedTouches[0].clientY;
	    if(ts > te+5){
	      numberUpMovil = numberUpMovil + 1;
	      if (numberUpMovil >= 8){
	        numberUpMovil = 0;
	        element = $('a.linkNav.active').parent().next();
	        $(element).find('a').trigger('click');
	      }
	    }else if(ts < te-5){
	      numberDownMovil = numberDownMovil + 1;
	      if (numberDownMovil >= 8){
	        numberDownMovil = 0;
	        element = $('a.linkNav.active').parent().prev();
	        $(element).find('a').trigger('click');
	      }
	    }
	    if ($('a.linkNav').length){
	      return false;
	    }
	});*/
	

})(jQuery);